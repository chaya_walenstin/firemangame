﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FiremanController : MonoBehaviour {

    float speed = 7f;
    float maxVel = 5f;
    int jumpForce;//Height the fireman is jumping

    Rigidbody2D rb2d;
    Animator animator;
    AudioSource audiosource;

    float levelRadius = 0.2f;
    public bool isJumpFromLevel;
    public LayerMask WhatIsLevelMask;
    public Transform[] levelPoints;
    public string gameOverSceneName;

    public AudioClip jumpPlayer;
    public AudioClip deadPlayer;
    public AudioClip pickupBoy;
    public AudioClip touchFire;
    public int boysRescue = 0;
    public int lives  ;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audiosource = GetComponent<AudioSource>();
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    }

    private void FixedUpdate()
    {
        PlayerMove();
    }
    
    bool checkedIsLevel()
    {
        for (int i = 0; i < levelPoints.Length; i++)
        {
            if (Physics2D.OverlapCircle(levelPoints[i].position, levelRadius, WhatIsLevelMask))
                return true;
        }

        return false;
    }
    void PlayerMove()
    {
        float x = 0f;
        float currvel = Mathf.Abs(rb2d.velocity.x);

        float motion = Input.GetAxisRaw("Horizontal");

        if (motion > 0)//right
        {
            animator.SetBool("Walk", true);
            if (currvel < maxVel)
                x = speed;
            Vector3 scale = transform.localScale;
            scale.x = 0.6f;
            transform.localScale = scale;
        }
        else if (motion < 0)//left
        {
            animator.SetBool("Walk", true);
            if (currvel < maxVel)
                x = -speed;
            Vector3 scale = transform.localScale;
            scale.x = -0.6f;
            transform.localScale = scale;
        }
        else//stop
            animator.SetBool("Walk", false);


        rb2d.AddForce(new Vector2(x, 0));

        isJumpFromLevel = checkedIsLevel();
        if (isJumpFromLevel && Input.GetButtonDown("Jump"))
        {
            rb2d.AddForce(new Vector2(x, 500));
            audiosource.PlayOneShot(jumpPlayer);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Boy")
        {
            audiosource.PlayOneShot(pickupBoy);
            boysRescue++;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "Fire")
        {
            audiosource.PlayOneShot(touchFire);
            lives--;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Finish")
            EndGame();
       
        
    }
    public void EndGame()
    {
        Time.timeScale = 0;
        // Destroy(playerController.gameObject);
        SceneManager.LoadScene(gameOverSceneName);

    }
}
