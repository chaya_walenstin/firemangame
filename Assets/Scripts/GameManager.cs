﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //public float timeOver;
    //public Text timeOverText;
    //public float timeFactor;
    public Image UIFire1;
    public Image UIFire2;
    public Image UIFire3;
    public Text rescueText;
    public FiremanController playerController;
    public string gameOverSceneName;

    // Use this for initialization
    void Start () {
        playerController = GameObject.FindWithTag("Player").GetComponent<FiremanController>();
    }
	
	// Update is called once per frame
	void Update () {
        rescueText.text = playerController.boysRescue.ToString() ;

        if (playerController.lives == 2)
            UIFire3.enabled = false;
        else if (playerController.lives == 1)
            UIFire2.enabled = false;
       
        if (playerController.lives <= 0)
            EndGame();

        // UpdateTimeOver();
    }

   
    public void EndGame()
    {
        Time.timeScale = 0;
       // Destroy(playerController.gameObject);
        SceneManager.LoadScene(gameOverSceneName);

    }

   
    //public void UpdateTimeOver()
    //{
    //    timeOver += Time.deltaTime * timeFactor;
    //    timeOverText.text = "Score : " + timeOver.ToString("F0");
    //}
}
