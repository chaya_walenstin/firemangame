﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameObject followMe;
    private Vector3 newPosition;
    private float space = 1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (followMe.transform.localScale.x > 0f)//right
            newPosition = new Vector3(followMe.transform.position.x + space, transform.position.y, transform.position.z);
        else if (followMe.transform.localScale.x < 0f)//left
            newPosition = new Vector3(followMe.transform.position.x - space, transform.position.y, transform.position.z);

        transform.position = newPosition;
    }
}
